/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shipshape.expenses.backend;


import com.shipshape.expenses.backend.core.Expense;
import com.shipshape.expenses.backend.dao.ExpenseDAO;
import com.shipshape.expenses.backend.resources.ExpensesResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

/**
 *
 * @author jmt
 */
public class ExpensesApplication extends Application<ExpensesConfiguration> {

    public static void main(String[] args) throws Exception {
        new ExpensesApplication().run(args);
    }

    private final HibernateBundle<ExpensesConfiguration> hibernateBundle
            = new HibernateBundle<ExpensesConfiguration>(Expense.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(ExpensesConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public void initialize(Bootstrap<ExpensesConfiguration> bootstrap) {

        bootstrap.addBundle(new MigrationsBundle<ExpensesConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(ExpensesConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new ViewBundle());
    }

    @Override
    public void run(ExpensesConfiguration expensesAppConfiguration, Environment environment) throws ClassNotFoundException {

        final ExpenseDAO notificationDAO = new ExpenseDAO(hibernateBundle.getSessionFactory());
        environment.jersey().register(new ExpensesResource(notificationDAO));
        

    }

}
