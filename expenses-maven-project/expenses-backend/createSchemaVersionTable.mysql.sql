CREATE TABLE expenses (
  uuid VARCHAR(43) NOT NULL,
  date VARCHAR(32) NOT NULL,
  reason VARCHAR(100) NOT NULL,
  amount INT NOT NULL
);

ALTER TABLE changelog ADD CONSTRAINT Pkexpenses PRIMARY KEY (uuid)
;
