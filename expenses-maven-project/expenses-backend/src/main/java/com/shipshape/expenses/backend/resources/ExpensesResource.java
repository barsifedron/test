/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shipshape.expenses.backend.resources;

import com.shipshape.expenses.backend.core.Expense;
import com.shipshape.expenses.backend.dao.ExpenseDAO;
import io.dropwizard.hibernate.UnitOfWork;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jmt
 */
@Path("/expenses")
@Produces(MediaType.APPLICATION_JSON)
public class ExpensesResource {

    private final ExpenseDAO expenseDAO;

    public ExpensesResource(ExpenseDAO expenseDAO) {
        this.expenseDAO = expenseDAO;
    }

    @GET
    @UnitOfWork
    public List<Expense> getNotifications() {
        return expenseDAO.findAll();
    }

    @POST
    @UnitOfWork
    // convenient to fill database
    public Expense save(Expense expense) {
        return expenseDAO.save(expense);
    }
}
