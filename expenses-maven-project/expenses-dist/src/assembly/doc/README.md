
                           An expense Service for the whole family
  _____  ___ __   ___ _ __  ___  ___  ___ 
 / _ \ \/ / '_ \ / _ \ '_ \/ __|/ _ \/ __|
|  __/>  <| |_) |  __/ | | \__ \  __/\__ \
 \___/_/\_\ .__/ \___|_| |_|___/\___||___/
          |_|                             




--------------------------------------------------------------------------------
----------------------------------- INTRODUCTION -------------------------------
--------------------------------------------------------------------------------


The expenses app comes with two modules :

- the expenses backend provide Rest Api's to register and acces expenses. It was 
built with drop wizard and uses an online mysql database

- the expenses frontend provides a web app allowing to register new expenses.



--------------------------- ZIP ARBORESCENCE

Here is a description of this Zip arborescence :

${expenses.dist.name}-dist.zip  
|-- README.md
|-- expenses-backend
|-- expenses-frontend



--------------------------------------------------------------------------------
------------------------------  GETTING STARTED --------------------------------
--------------------------------------------------------------------------------



------------------------- PREREQUESITES 

- The variable $JAVA_HOME must be set to a jre of version 7
- We will use the http ports 8080 and 9000 !!!!!!


-------------------------- INSTALLATION 

Start by unzipping ${expenses.dist.name}-dist.zip 

so you have the arborescence

`-${expenses.dist.name}/
|-- README.md
|-- expenses-backend
|-- expenses-frontend




--------------------- LAUNCHING THE EXPENSES-SERVICE 

In your shell, navigate to 

`-${expenses.dist.name}/
|-- expenses-backend

and type :    "sh start.sh"
This will launch the expenses-backend with the preconfigured online database.



--------------------- LAUNCHING THE WEBAPP


In your shell, navigate to 


`-${expenses.dist.name}/
|-- expenses-frontend
and type :    "sh start.sh"

This will launch the expenses webapp



----------------------- SEEING THAT IT WORKS


Open your web-browser and go to the adress :  http://localhost:8080/
You should see a list of users. Click on one of them to see all his/her notifications!


--------------------------------------------------------------------------------
---------------------------POINTS OF INTEREST-----------------------------------
--------------------------------------------------------------------------------



---------------------  ABOUT THE REST SERVICES

The wanderapp-service listens on the http port 9000.


For the REST Services, the following paths are proposed

   
    GET     /expenses
    POST    /expenses


----------------------------- ABOUT THE WEBAPP ------------------------------------

The webapp was built using angularjs.
It's hosted by a Jetty intance, with a proxy forwarding the REST request to the 
backend on port 9000.







------------------------------ABOUT THE DATABSE --------------------------------


We use a database hosted online on http://www.db4free.net/.
The build preconfigures the app to reach this database.
    


---- RINCE THE DATABASE AND RESTART


By connecting top http://www.db4free.net/ with the 

login : barsifedron
passsword : barsifedron

you suppress ALL the tables. Launching the expenses backend with the script 

"sh init.sh" and then "sh start.sh" will recreate the proper table to work with.

/!\ This will only work if you completely rinse the database.





--------------------------- THE MAVEN PROJECT ----------------------


The porject has 3 modules

        <module>expenses-backend</module>
        <module>expenses-frontend</module>
        <module>expenses-dist</module>


The module <module>expenses-dist</module> produces the distribution zip, 
ready to install and to play with.


