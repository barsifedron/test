/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shipshape.expenses.backend.core;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author jmt
 */
@Entity
@Table(name = "expenses")
@NamedQueries({
    @NamedQuery(
            name = "com.shipshape.expenses.backend.core.Expense.findAll",
            query = "SELECT e FROM Expense e ORDER BY date"
    )
})
public class Expense {

    @Id
    @Column(name = "expenseUuid", unique = true, nullable = false)
    private String expenseUuid = UUID.randomUUID().toString();

    @Column(name = "amount", unique = true, nullable = false)
    private double amount;

    @Column(name = "date", nullable = false)
    private String date;

    @Column(name = "reason", nullable = false)
    private String reason;

    public Expense() {
    }

    public Expense(double amount, String date, String reason) {
        this.amount = amount;
        this.date = date;
        this.reason = reason;
    }

    public String getExpenseUuid() {
        return expenseUuid;
    }

    public double getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public String getReason() {
        return reason;
    }

    public void setExpenseUuid(String expenseUuid) {
        this.expenseUuid = expenseUuid;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
