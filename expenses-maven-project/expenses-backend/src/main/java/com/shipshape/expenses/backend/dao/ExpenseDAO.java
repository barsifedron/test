/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shipshape.expenses.backend.dao;

import com.shipshape.expenses.backend.core.Expense;
import io.dropwizard.hibernate.AbstractDAO;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 *
 * @author jmt
 */
public class ExpenseDAO extends AbstractDAO<Expense> {

    public ExpenseDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Expense> findAll() {
        return list(namedQuery("com.shipshape.expenses.backend.core.Expense.findAll"));
    }

    public Expense save(Expense expense) {
        return persist(expense);
    }

}
